/**
 * MyoRemotControl
 *
 *  Copyright 2016 by Tim Dünte <tim.duente@hci.uni-hannover.de>
 *  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
 *
 *  Licensed under "The MIT License (MIT) – military use of this product is forbidden – V 0.2".
 *  Some rights reserved. See LICENSE.
 *
 * @license "The MIT License (MIT) – military use of this product is forbidden – V 0.2"
 * <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/License>
 */

/*
 * Copyright (C) 2014 Thalmic Labs Inc.
 * Distributed under the Myo SDK license agreement. See LICENSE.txt for details.
 * edited by Max Pfeiffer and Tim Duente 2015
 *
 */

package de.luh.hci.ems.myo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;


import com.thalmic.myo.AbstractDeviceListener;
import com.thalmic.myo.DeviceListener;
import com.thalmic.myo.Hub;
import com.thalmic.myo.Myo;
import com.thalmic.myo.Pose;
import com.thalmic.myo.Quaternion;
import com.thalmic.myo.XDirection;
import com.thalmic.myo.scanner.ScanActivity;

import android.app.AlertDialog;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;


//import com.example.bluetoothle.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import de.luh.hci.ems.ble.IEMSBluetoothLEService;
import de.luh.hci.ems.commands.EMSModule;
import de.luh.hci.ems.commands.IEMSModule;

public class MyoEmsActivity extends Activity implements OnTouchListener, Observer {

    private Button buttonRightOn;
    private Button buttonLeftOn;
    private RadioGroup radiosLeft;
    private RadioGroup radiosRight;

    private String greyColor = "#ffeaf6ff";

    private String configFileName = "config.txt";
    private File configFile;


    private IEMSModule emsCommandManager;

    private String deviceName = "EMS08IK";

    private static final int REQUEST_ENABLE_BT = 1;

    AlertDialog.Builder alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_myo);
        Hub hub = Hub.getInstance();

        if (!hub.init(this, getPackageName())) {
            Toast.makeText(this, "Couldn't initialize Myo", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        hub.addListener(mListener);
        if (!getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT)
                    .show();
            finish();
        }

        configFile = new File(this.getFilesDir(), configFileName);

        if (configFile.exists()) {
            try {
                FileReader reader = new FileReader(configFile);
                BufferedReader bufferedReader = new BufferedReader(reader);
                deviceName = bufferedReader.readLine();
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            writeDeviceNameToConfigFile();
        }

        emsCommandManager = new EMSModule((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE), deviceName);
        emsCommandManager.getBluetoothLEConnector().addObserver(this);
        emsCommandManager.connect();

        buttonRightOn = (Button) findViewById(R.id.buttonRight);
        buttonLeftOn = (Button) findViewById(R.id.buttonLeft);

        buttonRightOn.setOnTouchListener(this);
        buttonLeftOn.setOnTouchListener(this);

        SeekBar intensityLeft = (SeekBar) findViewById(R.id.seekBarLeft);
        SeekBar intensityRight = (SeekBar) findViewById(R.id.seekBarRight);

        intensityLeft.setLeft(0);
        intensityLeft.setRight(100);
        intensityLeft.setProgress(100);
        intensityRight.setLeft(0);
        intensityRight.setRight(100);
        intensityRight.setProgress(100);

        intensityLeft.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int channel = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Change intensity of channel 0
                emsCommandManager.setMAX_INTENSITY(progress, channel);
                emsCommandManager.setIntensity(progress, channel);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        intensityRight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int channel = 1;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                //Change intensity of channel 1
                emsCommandManager.setMAX_INTENSITY(progress, channel);
                emsCommandManager.setIntensity(progress, channel);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        alert = new AlertDialog.Builder(this);

        alert.setTitle("Enter device name");
        alert.setMessage("Enter the name of the device you wish to connect.");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);

        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                deviceName = input.getText().toString();
                emsCommandManager.setDeviceName(deviceName);
                writeDeviceNameToConfigFile();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Hub.getInstance().removeListener(mListener);

        if (isFinishing()) {
            Hub.getInstance().shutdown();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (deviceName != null) {
            menu.getItem(1).setTitle("Current Device: " + deviceName);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (R.id.action_scan == id) {
            onScanActionSelected();
            return true;
        }
        if (id == R.id.action_connect) {
            emsCommandManager.connect();
            return true;
        }
        if (id == R.id.action_settings) {
            alert.show();
        }
        if (id == R.id.action_disconnect) {
            emsCommandManager.disconnect();
        }

        return super.onOptionsItemSelected(item);
    }

    private void onScanActionSelected() {
        Intent intent = new Intent(this, ScanActivity.class);
        startActivity(intent);
    }

    private void writeDeviceNameToConfigFile() {
        try {
            FileWriter fileWriter = new FileWriter(configFile);
            fileWriter.write(deviceName + "\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private DeviceListener mListener = new AbstractDeviceListener() {

        @Override
        public void onOrientationData(Myo myo, long timestamp, Quaternion rotation) {
            // Calculate Euler angles (roll, pitch, and yaw) from the quaternion.
            float roll = (float) Math.toDegrees(Quaternion.roll(rotation));
            float pitch = (float) Math.toDegrees(Quaternion.pitch(rotation));
            float yaw = (float) Math.toDegrees(Quaternion.yaw(rotation));

            // Adjust roll and pitch for the orientation of the Myo on the arm.
            if (myo.getXDirection() == XDirection.TOWARD_ELBOW) {
                roll *= -1;
                pitch *= -1;
            }

        }

        @Override
        public void onPose(Myo myo, long timestamp, Pose pose) {
            // Handle the cases of the Pose enumeration
            switch (pose) {
                case UNKNOWN:
                    /*if(emsCommandManager!=null) {
                        emsCommandManager.stopCommand(0);

                    }
                    if(emsCommandManager!=null) {
                        emsCommandManager.stopCommand(1);

                    }
                    */
                    break;
                case REST:
                case DOUBLE_TAP:

                    break;
                case FIST:

                    break;
                case WAVE_IN:
                    if(emsCommandManager!=null)
                        emsCommandManager.sendMessageToBoard("C0I100T1000G");

                    break;
                case WAVE_OUT:
                    if(emsCommandManager!=null)
                        emsCommandManager.sendMessageToBoard("C1I100T1000G");

                    break;
                case FINGERS_SPREAD:

                    break;
            }

            if (pose != Pose.UNKNOWN && pose != Pose.REST) {
                // Tell the Myo to stay unlocked until told otherwise. We do that here so you can
                // hold the poses without the Myo becoming locked.
                myo.unlock(Myo.UnlockType.HOLD);

                /* if(emsCommandManager!=null) {
                    emsCommandManager.stopCommand(0);

                }

                if(emsCommandManager!=null) {
                    emsCommandManager.stopCommand(1);

                }
                */

                myo.notifyUserAction();
            } else {
                myo.unlock(Myo.UnlockType.TIMED);
            }
        }
    };


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (v == buttonRightOn) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    emsCommandManager.startCommand(1);


                buttonRightOn.setBackgroundColor(Color.RED);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {

                if (emsCommandManager.getPattern(1)==5) {
                }else {

                    emsCommandManager.stopCommand(1);
                }
                buttonRightOn.setBackgroundColor(Color.GREEN);
            }
        } else if (v == buttonLeftOn) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {

                    emsCommandManager.startCommand(0);

                buttonLeftOn.setBackgroundColor(Color.RED);
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                if (emsCommandManager.getPattern(0)==5) {
                }else {
                    emsCommandManager.stopCommand(0);
                }
                buttonLeftOn.setBackgroundColor(Color.GREEN);
            }
        }

        v.performClick();
        return false;
    }

    @Override
    public void update(Observable observable, Object data) {
        final IEMSBluetoothLEService bleConnector = emsCommandManager.getBluetoothLEConnector();
        if (data == bleConnector) {
            System.out.println("Worked: " + bleConnector.isConnected());

            this.runOnUiThread(new Runnable() {
                                   @Override
                                   public void run() {
                                       if (bleConnector.isConnected()) {
                                           buttonLeftOn.setEnabled(true);
                                           buttonLeftOn.setBackgroundColor(Color.GREEN);
                                           buttonRightOn.setEnabled(true);
                                           buttonRightOn.setBackgroundColor(Color.GREEN);
                                           buttonLeftOn.invalidate();
                                           buttonRightOn.invalidate();
                                       } else {
                                           buttonLeftOn.setEnabled(false);
                                           buttonLeftOn.setBackgroundColor(Color.parseColor(greyColor));
                                           buttonRightOn.setEnabled(false);
                                           buttonRightOn.setBackgroundColor(Color.parseColor(greyColor));
                                           buttonLeftOn.invalidate();
                                           buttonRightOn.invalidate();
                                       }
                                   }
                               }
            );

        }
    }

}
