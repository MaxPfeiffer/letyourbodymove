/**
 * Multiple Devices
 *
 *  Copyright 2016 by Tim Dünte <tim.duente@hci.uni-hannover.de>
 *  Copyright 2016 by Max Pfeiffer <max.pfeiffer@hci.uni-hannover.de>
 *
 *  Licensed under "The MIT License (MIT) – military use of this product is forbidden – V 0.2".
 *  Some rights reserved. See LICENSE.
 *
 * @license "The MIT License (MIT) – military use of this product is forbidden – V 0.2"
 * <https://bitbucket.org/MaxPfeiffer/letyourbodymove/wiki/Home/License>
 */

package de.luh.hci.ems.commands;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by pfeiffer on 28.08.15.
 */

public class UDPReceiver extends Thread {
    private boolean bKeepRunning = true;
    private String lastMessage = "";
    private static final int UDP_SERVER_PORT = 2000;
    private static final int MAX_UDP_DATAGRAM_LEN = 1500;

    public void run() {
        String message;
        byte[] lmessage = new byte[MAX_UDP_DATAGRAM_LEN];
        DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);
        DatagramSocket socket = null;
        Log.w("UDP", "Start UDP");
        try {
            socket = new DatagramSocket(UDP_SERVER_PORT);

            while (bKeepRunning) {

                socket.receive(packet);
                message = new String(lmessage, 0, packet.getLength());
                lastMessage = message;
                //runOnUiThread(updatUDPMessage);

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        if (socket != null) {
            socket.close();
        }
    }

    public void kill() {
        bKeepRunning = false;
    }

    public String getLastMessage() {
        return lastMessage;
    }
}


