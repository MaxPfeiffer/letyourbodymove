# Building a case for your EMS board
In this repo you have a files for:

1. a 3D printed case
2. a laser-cuttable box case (with fingerjoints)

## 3D printed version

<img src="https://bytebucket.org/MaxPfeiffer/letyourbodymove/raw/fc38b83d263306931eb49d0ea7c2dd3e3238d5fc/ToolKit/Casings/3dPrintedCasing/3dPrintedCasing.jpg?token=beccbbfc5b182da59c808756dcbee6beafea2bde" width="400">


This version is straightforward: simply print it and it all should fit as illustrated below.

## laser cut box

<img src="https://bytebucket.org/MaxPfeiffer/letyourbodymove/raw/fc38b83d263306931eb49d0ea7c2dd3e3238d5fc/ToolKit/Casings/Lasercut/lasercut_casing.jpg?token=dd0b68c23c33d034c44ae358ef1e98b452aecaa4" width="400">

Don't forget to test the fingerjoints on your cutter first, since if you want these to click properly they might require adjustment. If not simply glue the box using acrylic or wood glue (based on your choice of material). The box will look similar to the one below.

## make your own or remix it!
The files here are (just like the rest of this project) entirely open source, so feel free to remix and share with others what you have created. For instance: if your arduino nano does not have a top programming header, you can cut the top of the case without the hole. Or if you use another battery with this board (refer to hardware modifications) you can resize the box to fit a smaller li-po, etc. 





